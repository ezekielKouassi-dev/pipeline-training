package com.eburtis.pipelinetraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PipelineTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineTrainingApplication.class, args);
	}

}
